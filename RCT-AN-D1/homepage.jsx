import React, { Component } from "react";
import Carousel from "./carousel";
import { url } from "./config.json";
import http from "./httpService";
import { paginate } from "./paginate";
class HomePage extends Component {
  state = {
    data: [],
    currentPage: 1,
    pageSize: 5
  };

  async componentDidMount() {
    let endpoint = url + "/deals";
    try {
      let { data } = await http.get(endpoint);
      console.log(data);
      this.setState({ data });
    } catch (error) {
      console.log(error);
    }
  }
  handlePage = value => {
    let { currentPage } = this.state;
    currentPage = currentPage + value;
    this.setState({ currentPage });
  };
  handleClickOnMobile = mobile => {
    let path = "/mobiles/" + mobile.brand + "/" + mobile.id;
    console.log("path", path);
    this.props.history.push(path);
  };
  handleThisMobile = () => {
    this.props.history.push("/mobiles/RealMe/M20");
  };
  render() {
    let { data: allData, currentPage, pageSize } = this.state;
    let data = paginate(allData, currentPage, pageSize);
    return (
      <div className="container-fluid" style={{ background: "lightgrey" }}>
        <Carousel />
        <div className="row mt-1">
          <div
            className="col-lg-9 col-12 border border-bottom ml-0"
            style={{
              backgroundColor: "white",
              fontSize: "22px",
              lineHeight: "32px"
            }}
          >
            <b>Deals of the Day</b>
            <hr />
            <div className="row">
              {currentPage > 1 ? (
                <div className="col-1 text-left">
                  <button
                    style={{
                      alignSelf: "center",
                      padding: "60px 5px",
                      color: "black",
                      background: "white",
                      cursor: "pointer",
                      display: "flex",
                      position: "absolute",
                      zIndex: "1"
                    }}
                    onClick={() => this.handlePage(-1)}
                  >
                    <i className="fa fa-angle-left"></i>
                  </button>
                </div>
              ) : (
                ""
              )}
              {data &&
                data.map((n1, i) => (
                  <div className="col-lg-2 col-3 text-center" key={i}>
                    <div className="row ml-lg-3">
                      <img
                        style={{ height: "150px", cursor: "pointer" }}
                        src={n1.img}
                        onClick={() => this.handleClickOnMobile(n1)}
                      />
                    </div>
                    <div
                      className="row p-0"
                      style={{
                        fontSize: "12px",
                        fontWeight: "500",
                        marginTop: "15px"
                      }}
                    >
                      <div className="text-truncate">{n1.name}</div>
                    </div>
                    <div id="abc" className="row">
                      <div
                        id="abc"
                        className="col"
                        style={{ color: "#388e3c", fontSize: "10px" }}
                      >
                        {n1.discount} % off
                      </div>
                    </div>
                  </div>
                ))}
              {data.length === 5 ? (
                <div className="col-1 text-right">
                  <button
                    style={{
                      alignSelf: "center",
                      padding: "60px 5px",
                      color: "black",
                      background: "white",
                      cursor: "pointer",
                      display: "flex",
                      position: "absolute",
                      zIndex: "1"
                    }}
                    onClick={() => this.handlePage(1)}
                  >
                    <i className="fa fa-angle-right"></i>
                  </button>
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
          <div className="col-2 border-light ml-1 bg-white text-right d-none d-lg-block">
            <img
              className="img-fluid"
              style={{ width: "auto" }}
              src="https://i.ibb.co/1GBrRnn/fa04c5362949d9f1.jpg"
            />
          </div>
        </div>
        <div className="row ml-1 mt-1">
          <div className="col-4 text-center">
            <img
              className="img-fluid"
              style={{ width: "auto", height: "auto" }}
              onClick={() => this.handleThisMobile()}
              src="https://i.ibb.co/dPVHZGW/d5db30a716f82657.jpg"
            />
          </div>
          <div className="col-4 text-center">
            <img
              className="img-fluid"
              style={{ width: "auto" }}
              src="https://i.ibb.co/Lzz36zB/31efaad41a3e4208.jpg"
            />
          </div>
          <div className="col-4 text-center">
            <img
              className="img-fluid"
              style={{ width: "auto" }}
              src="https://i.ibb.co/fGX7sFh/4e219998fadcbc70.jpg"
            />
          </div>
        </div>

        <div
          className="row border-top mt-2"
          style={{ backgroundColor: "#fff", color: "#848484" }}
        >
          <div className="col mt-3" style={{ fontSize: "10px" }}>
            <h6>Flipkart: The One-stop Shopping Destination</h6>
            <p>
              E-commerce is revolutionizing the way we all shop in India. Why do
              you want to hop from one store to another in search of the latest
              phone when you can find it on the Internet in a single click? Not
              only mobiles. Flipkart houses everything you can possibly imagine,
              from trending electronics like laptops, tablets, smartphones, and
              mobile accessories to in-vogue fashion staples like shoes,
              clothing and lifestyle accessories; from modern furniture like
              sofa sets, dining tables, and wardrobes to appliances that make
              your life easy like washing machines, TVs, ACs, mixer grinder
              juicers and other time-saving kitchen and small appliances; from
              home furnishings like cushion covers, mattresses and bedsheets to
              toys and musical instruments, we got them all covered. You name
              it, and you can stay assured about finding them all here. For
              those of you with erratic working hours, Flipkart is your best
              bet. Shop in your PJs, at night or in the wee hours of the
              morning. This e-commerce never shuts down.{" "}
            </p>
          </div>
        </div>
        <div
          className="row"
          style={{ backgroundColor: "#fff", color: "#848484" }}
        >
          <div className="col" style={{ fontSize: "10px" }}>
            <h6>Flipkart Plus</h6>
            <p>
              A world of limitless possibilities awaits you - Flipkart Plus was
              kickstarted as a loyalty reward programme for all its regular
              customers at zero subscription fee. All you need is 500 supercoins
              to be a part of this service. For every 100 rupees spent on
              Flipkart order, Plus members earns 4 supercoins & non-plus members
              earn 2 supercoins. Free delivery, early access during sales and
              shopping festivals, exchange offers and priority customer service
              are the top benefits to a Flipkart Plus member. In short, earn
              more when you shop more! What's more, you can even use the
              Flipkart supercoins for a number of exciting services, like: An
              annual Zomato Gold membership An annual Hotstar Premium membership
              6 months Gaana plus subscription Rupees 550 instant discount on
              flights on ixigo Check out
              https://www.flipkart.com/plus/all-offers for the entire list.
              Terms and conditions apply.{" "}
            </p>
          </div>
        </div>

        <div
          className="row"
          style={{ backgroundColor: "#fff", color: "#848484" }}
        >
          <div className="col" style={{ fontSize: "10px" }}>
            <h6>No Cost EMI</h6>
            <p>
              In an attempt to make high-end products accessible to all, our No
              Cost EMI plan enables you to shop with us under EMI, without
              shelling out any processing fee. Applicable on select mobiles,
              laptops, large and small appliances, furniture, electronics and
              watches, you can now shop without burning a hole in your pocket.
              If you've been eyeing a product for a long time, chances are it
              may be up for a no cost EMI. Take a look ASAP! Terms and
              conditions apply.{" "}
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default HomePage;
