import React, { Component } from "react";
class LaptopLeftPanel extends Component {
  state = {
    position: "up",
    ratingPosition: "up",
    pricePosition: "up"
  };
  handleChange = e => {
    const { currentTarget: input } = e;
    let { priceCheckBox, ratingCheckBox, assured } = this.props;
    assured = input.checked;

    this.props.onOptionChanges(priceCheckBox, ratingCheckBox, assured);
  };
  handleChange2 = e => {
    const { currentTarget: input } = e;
    let { priceCheckBox, ratingCheckBox, assured } = this.props;

    let cb = ratingCheckBox.filter(n1 => n1.name === input.name);
    if (cb) cb.map(n1 => (n1.selected = input.checked));

    this.props.onOptionChanges(priceCheckBox, ratingCheckBox, assured);
  };

  handleChange3 = e => {
    const { currentTarget: input } = e;
    let { priceCheckBox, ratingCheckBox, assured } = this.props;

    let cb = priceCheckBox.filter(n1 => n1.name === input.name);
    if (cb) cb.map(n1 => (n1.selected = input.checked));

    this.props.onOptionChanges(priceCheckBox, ratingCheckBox, assured);
  };

  handlePositionEvent = value => {
    this.setState({ position: value });
  };
  handlePricePostions = value => {
    this.setState({ pricePosition: value });
  };
  handleRatingPosition = value => {
    this.setState({ ratingPosition: value });
  };
  render() {
    let { priceCheckBox, ratingCheckBox, assured } = this.props;
    let { position, ratingPosition, pricePosition } = this.state;
    return (
      <div>
        <div className="row bg-white border-bottom pt-2 pb-2">
          <div
            className="col"
            style={{
              fontSize: "18px",
              textTransform: "capitalize",
              width: "67%",
              fontWeight: "500"
            }}
          >
            Filters
          </div>
        </div>
        <div className="row bg-white pt-2 pb-2">
          <div className="col-9">
            <div className="checkbox">
              <label>
                <input
                  type="ng-untouched ng-pristine ng-valid"
                  type="checkbox"
                  name="assured"
                  value={assured}
                  checked={assured}
                  onChange={this.handleChange}
                />{" "}
                <img
                  src="https://i.ibb.co/t8bPSBN/fa-8b4b59.png"
                  style={{ width: "80px", height: "21px" }}
                />
              </label>
            </div>
          </div>
          <div className="col-3">
            <span
              className="border"
              style={{
                borderRadius: "50%",
                width: "16px",
                height: "16px",
                fontSize: "11px",
                lineHeight: "16px",
                textAlign: "center",
                fontWeight: "500",
                color: "#878787",
                display: "inline-block",
                border: "1px solid",
                boxShadow: "0 0 1px 0 rgba(0,0,0,.2)",
                verticalAlign: "middle",
                magin: "0,2px, 0, 8px",
                cursor: "pointer"
              }}
            >
              ?
            </span>
          </div>
        </div>

        <div className="row border-top bg-white">
          <div className="col-10">
            <div
              className="row ml-1 pb-2"
              style={{
                fontSize: "13px",
                fontWeight: "500",
                textTransform: "uppercase",
                letterSpacing: ".3px",
                display: "inline-block"
              }}
            >
              Customer Rating
            </div>
            <div>
              {ratingCheckBox.map((n1, i) => (
                <div className="row form-check ml-1 pb-1" key={i}>
                  <div
                    className="checkbox"
                    style={{
                      verticalAlign: "middle",
                      fontSize: "14px",
                      paddingLeft: "11px",
                      textOverflow: "ellipsis",
                      lineHeight: "1",
                      cursor: "pointer"
                    }}
                  >
                    {ratingPosition === "up" ? (
                      <label htmlFor={n1.name}>
                        <input
                          type="checkbox"
                          id={n1.name}
                          className="ng-valid ng-dirty ng-touched"
                          value={n1.value}
                          checked={n1.selected}
                          name={n1.name}
                          onChange={this.handleChange2}
                        />{" "}
                        {n1.name}
                      </label>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div className="col-2 text-right">
            <span>
              {ratingPosition === "down" ? (
                <i
                  className="fa fa-chevron-down"
                  onClick={() => this.handleRatingPosition("up")}
                  style={{
                    fontSize: "10px",
                    color: "lightgrey",
                    cursor: "pointer"
                  }}
                ></i>
              ) : (
                <i
                  className="fa fa-chevron-up"
                  onClick={() => this.handleRatingPosition("down")}
                  style={{
                    fontSize: "10px",
                    color: "lightgrey",
                    cursor: "pointer"
                  }}
                ></i>
              )}
            </span>
          </div>
        </div>
        <div className="row border-top bg-white">
          <div className="col-10">
            <div
              className="row ml-1 pb-2"
              style={{
                fontSize: "13px",
                fontWeight: "500",
                textTransform: "uppercase",
                letterSpacing: ".3px",
                display: "inline-block"
              }}
            >
              Price
            </div>
            <div>
              {priceCheckBox.map((n1, i) => (
                <div className="row form-check ml-1 pb-1" key={i}>
                  <div
                    className="checkbox"
                    style={{
                      verticalAlign: "middle",
                      fontSize: "14px",
                      paddingLeft: "11px",
                      textOverflow: "ellipsis",
                      lineHeight: "1",
                      cursor: "pointer"
                    }}
                  >
                    {pricePosition === "up" ? (
                      <label htmlFor={n1.name}>
                        <input
                          type="checkbox"
                          id={n1.name}
                          className="ng-valid ng-dirty ng-touched"
                          value={n1.value}
                          checked={n1.selected}
                          name={n1.name}
                          onChange={this.handleChange3}
                        />{" "}
                        {n1.name}
                      </label>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div className="col-2 text-right">
            <span>
              {pricePosition === "down" ? (
                <i
                  className="fa fa-chevron-down"
                  onClick={() => this.handlePricePostions("up")}
                  style={{
                    fontSize: "10px",
                    color: "lightgrey",
                    cursor: "pointer"
                  }}
                ></i>
              ) : (
                <i
                  className="fa fa-chevron-up"
                  onClick={() => this.handlePricePostions("down")}
                  style={{
                    fontSize: "10px",
                    color: "lightgrey",
                    cursor: "pointer"
                  }}
                ></i>
              )}
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default LaptopLeftPanel;
