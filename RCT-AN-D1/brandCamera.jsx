import React, { Component } from "react";
import queryString from "query-string";
import { url2 } from "./config2.json";
import http from "./httpService";
import "./namecss.css";
import { Link } from "react-router-dom";
import LaptopLeftPanel from "./laptopLeftpanel";
class BrandCamera extends Component {
  state = {
    assure: false,
    ratings: [
      { name: "4★ & above", value: "4" },
      { name: "3★ & above", value: "3" },
      { name: "2★ & above", value: "2" },
      { name: "1★ & above", value: "1" }
    ],
    prices: [
      { name: "0-10,000", value: "0-10000" },
      { name: "10,000-20,000", value: "10000-20000" },
      { name: "30,000-50,000", value: "30000-50000" },
      { name: "More than 50,000", value: "More than 50000" }
    ],
    data: [],
    currentPage: 1,
    heart: []
  };
  async componentDidMount() {
    let brand = this.props.match.params.brand;
    let search = this.props.location.search;
    console.log("brand", brand);
    let endpoint;
    if (brand) {
      this.setState({ brand });
      endpoint = url2 + "/camera/" + brand + search;
    } else {
      endpoint = url2 + "/camera" + search;
    }
    console.log(endpoint);
    try {
      let { data } = await http.get(endpoint);
      console.log("data", data);
      this.setState({ data });
    } catch (error) {
      console.log(error);
    }
  }
  async componentDidUpdate(prevProps, prevState) {
    let path1 = prevProps.match;
    let path2 = this.props.match;
    let brand = this.props.match.params.brand;
    let search = this.props.location.search;
    let endpoint;
    if (brand) endpoint = url2 + "/camera/" + brand + search;
    else {
      endpoint = url2 + "/camera" + search;
    }
    if (path1 !== path2) {
      try {
        this.setState({ brand });
        let { data } = await http.get(endpoint);
        console.log("data", data);
        this.setState({ data });
      } catch (error) {
        console.log(error);
      }
    }
  }
  makeCbStructure(option, value) {
    let temp = option.map(n1 => ({
      name: n1.name,
      value: n1.value,
      selected: false
    }));
    let cnames = value.split(",");
    for (let i = 0; i < cnames.length; i++) {
      let obj = temp.find(n1 => n1.value === cnames[i]);
      if (obj) obj.selected = true;
    }
    return temp;
  }

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  handleOnOptionChange = (priceCheckBox, ratingCheckBox, assured) => {
    let filteredPrice = priceCheckBox.filter(n1 => n1.selected);
    let arrayName1 = filteredPrice.map(n1 => n1.value);
    let price = arrayName1.join(",");

    let filteredRating = ratingCheckBox.filter(n1 => n1.selected);
    let arrayName2 = filteredRating.map(n1 => n1.value);
    let rating = arrayName2.join(",");

    this.setState({ currentPage: 1 });
    this.callUrl("", price, rating, assured);
  };
  callUrl = (params, price, rating, assured) => {
    let brand = this.props.match.params.brand;
    let path;
    if (brand) path = "/camera/" + brand;
    else path = "/camera";
    let { q } = queryString.parse(this.props.location.search);

    console.log("path", path);
    console.log("parmas url", params);

    params = this.addToParams(params, "rating", rating);
    params = this.addToParams(params, "price", price);
    params = this.addToParams(params, "assured", assured);
    params = this.addToParams(params, "q", q);
    console.log("parmas url", params);
    this.props.history.push({
      pathname: path,
      search: params
    });
  };
  handlePages = pages => {
    let { currentPage, brand } = this.state;
    let arr = [];
    for (let i = 0; i < pages; i++) {
      arr[i] = i + 1;
    }
    console.log(arr);
    return (
      <div>
        {arr.map((n1, i) => (
          <span
            key={i}
            onClick={() => this.goToPage(n1)}
            style={
              currentPage === n1
                ? {
                    display: "inline-block",
                    padding: "0, 8px",
                    cursor: "pointer",
                    lineHeight: "32px",
                    height: "32px",
                    width: "32px",
                    borderRadius: "50%",
                    background: "#2874f0",
                    color: "#fff",
                    textAlign: "center",
                    fontWeight: "500"
                  }
                : {}
            }
          >
            {" "}
            {n1}{" "}
          </span>
        ))}
        {currentPage !== arr[arr.length - 1] ? (
          <sapn className="text-primary" onClick={() => this.handleNextPage(1)}>
            Next
          </sapn>
        ) : (
          ""
        )}
      </div>
    );
  };
  goToPage = x => {
    console.log("page", x);
    this.setState({ currentPage: x });
    let { ram } = this.state;
    let { assured, rating, price } = queryString.parse(
      this.props.location.search
    );
    assured = assured ? assured : false;
    rating = rating ? rating : "";

    price = price ? price : "";
    let params = this.addToParams("", "page", x);
    this.callUrl(params, price, rating, assured);
  };
  handleNextPage = x => {
    let { assured, rating, price, page } = queryString.parse(
      this.props.location.search
    );

    assured = assured ? assured : false;
    rating = rating ? rating : "";
    price = price ? price : "";
    let currentPage = page ? +page : 1;
    currentPage = currentPage + x;
    console.log("currentPage", currentPage);
    this.setState({ currentPage });
    let params = this.addToParams("", "page", currentPage);
    console.log("params", params);
    this.callUrl(params, price, rating, assured);
  };
  handleChangeCurrentPage = () => {
    this.setState({ currentPage: 1 });
  };
  handleHeartColor = item => {
    let { heart } = this.state;
    let index = heart.findIndex(n1 => n1 === item);
    if (index === -1) {
      heart.push(item);
    } else {
      heart.splice(index, 1);
    }
    this.setState(heart);
  };
  handleSelectMobile = mobile => {
    let path = "/camera/" + mobile.brand + "/" + mobile.id;
    console.log("path", path);
    this.props.history.push(path);
  };
  handleSort = value => {
    let { assured, rating, price, page } = queryString.parse(
      this.props.location.search
    );

    assured = assured ? assured : false;
    rating = rating ? rating : "";
    price = price ? price : "";
    page = page ? page : 1;
    let params = this.addToParams("", "page", page);
    params = this.addToParams("", "sort", value);
    this.callUrl(params, price, rating, assured);
  };
  render() {
    let { assure, ratings, prices, data, brand, heart } = this.state;
    let { assured, rating, price, sort } = queryString.parse(
      this.props.location.search
    );
    assured = assured ? assured : false;
    rating = rating ? rating : "";
    price = price ? price : "";
    sort = sort ? sort : "";

    let ratingCheckBox = this.makeCbStructure(ratings, rating);
    let priceCheckBox = this.makeCbStructure(prices, price);
    return (
      <div className="container-fluid" style={{ background: "lightgrey" }}>
        <div className="row mt-2 pt-2">
          <div className="col-2 ml-2 d-none d-lg-block">
            <LaptopLeftPanel
              assured={assured}
              ratingCheckBox={ratingCheckBox}
              priceCheckBox={priceCheckBox}
              onOptionChanges={this.handleOnOptionChange}
            />
          </div>
          <div className="col-lg-9 col-12 bg-white ml-1">
            <div className="row">
              <div className="col">
                <nav
                  aria-label="breadcrumb"
                  style={{ fontSize: "10px", backgroundColor: "white" }}
                >
                  <ol className="breadcrumb bg-white">
                    <li className="breadcrumb-item">
                      <Link to="#">Home</Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link
                        to="/camera"
                        onClick={() => this.handleChangeCurrentPage()}
                      >
                        Camera
                      </Link>
                    </li>
                    <li className="breadcrumb-item active" aria-current="page">
                      <Link to="#">{brand}</Link>
                    </li>
                  </ol>
                </nav>
              </div>
            </div>
            <div className="row">
              <div className="col">{brand} Camera</div>
            </div>
            <div
              className="row pb-1 border-bottom"
              style={{ fontSize: "14px" }}
            >
              <div className="col-2 d-none d-lg-block">
                <strong>Sort By</strong>
              </div>
              <div
                className={
                  sort === "popularity"
                    ? "col-1 d-none d-lg-block text-primary"
                    : "col-1 d-none d-lg-block"
                }
                style={{ cursor: "pointer", borderBottom: "5px" }}
                onClick={() => this.handleSort("popularity")}
              >
                Popularity
              </div>
              <div
                className={
                  sort === "desc"
                    ? "col-2 d-none d-lg-block text-primary"
                    : "col-2 d-none d-lg-block"
                }
                style={{ cursor: "pointer" }}
                onClick={() => this.handleSort("desc")}
              >
                Price High to Low
              </div>
              <div
                className={
                  sort === "asc"
                    ? "col-2 d-none d-lg-block text-primary"
                    : "col-2 d-none d-lg-block"
                }
                onClick={() => this.handleSort("asc")}
              >
                Price Low to High
              </div>
            </div>

            {data.data &&
              data.data.map((n1, i) => (
                <div key={i}>
                  <div className="row">
                    <div className="col-lg-2 col-9 text-center">
                      <img
                        style={{
                          height: "125px",
                          width: "150px",
                          cursor: "pointer"
                        }}
                        onClick={() => this.handleSelectMobile(n1)}
                        src={n1.img}
                      />
                    </div>
                    <div className="col-lg-1 col-2 text-secondary">
                      <i
                        className="fa fa-heart"
                        style={
                          heart.find(t1 => t1 === n1) === undefined
                            ? {}
                            : { color: "red" }
                        }
                        onClick={() => this.handleHeartColor(n1)}
                      ></i>
                    </div>
                    <div className="col-lg-5 col-12 text-left">
                      <div className="row">
                        <div
                          className="col"
                          style={{ fontSize: "16px", cursor: "pointer" }}
                        >
                          <span
                            id="namecss"
                            onClick={() => this.handleSelectMobile(n1)}
                          >
                            {n1.name}
                          </span>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col">
                          <span
                            style={{
                              lineHeight: "normal",
                              display: "inline-block",
                              color: "#fff",
                              padding: "2px 4px 2px 6px",
                              borderRadius: "3px",
                              fontWeight: "500",
                              fontSize: "12px",
                              verticalAlign: "middle",
                              backgroundColor: "#388e3c"
                            }}
                          >
                            <strong>
                              {n1.rating}
                              <img
                                src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwM
C9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQ
zOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEyb
DEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                              />
                            </strong>
                          </span>{" "}
                          <span className="text-muted">{n1.ratingDesc}</span>
                        </div>
                      </div>
                      {n1.details.map((m1, index) => (
                        <ul
                          key={index}
                          style={{
                            color: "#c2c2c2",
                            fontSize: "12px",
                            display: "inline",
                            padding: "0%"
                          }}
                        >
                          <li>{m1}</li>
                        </ul>
                      ))}
                    </div>
                    <div className="col-lg-3 col-12">
                      <div className="row">
                        <div className="col" style={{ fontSize: "25px" }}>
                          <strong>&#8377;{n1.price}</strong>{" "}
                          <span>
                            <img
                              className="img-fluid"
                              style={{ width: "70px" }}
                              src="https://i.ibb.co/t8bPSBN/fa-8b4b59.png"
                            />
                          </span>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col">
                          <span
                            style={{
                              textDecoration: "line-through",
                              fontSize: "14px",
                              color: "#878787"
                            }}
                          >
                            {n1.prevPrice}
                          </span>{" "}
                          <span
                            style={{
                              color: "#388e3c",
                              fontSize: "13px",
                              fontWeight: "500"
                            }}
                          >
                            {n1.discount}%
                          </span>
                        </div>
                      </div>
                      <div className="row" style={{ fontSize: "14px" }}>
                        <div className="col">No Cost EMI</div>
                      </div>
                      <div className="row" style={{ fontSize: "14px" }}>
                        {n1.exchange}
                      </div>
                    </div>
                  </div>
                  <hr />
                </div>
              ))}
            <div className="row">
              {data.pageInfo && data.pageInfo.pageNumber ? (
                <div className="col-lg-2 col-4">
                  Page {data.pageInfo && data.pageInfo.pageNumber} of{" "}
                  {data.pageInfo && data.pageInfo.numberOfPages}
                </div>
              ) : (
                ""
              )}
              <div
                className="col-8 p-0 text-center"
                style={{
                  cursor: "pointer",
                  lineHeight: "32px",
                  height: "32px",
                  width: "32px",
                  borderRadius: "50%"
                }}
              >
                {this.handlePages(data.pageInfo && data.pageInfo.numberOfPages)}
              </div>
            </div>
            {/*col-9 */}
          </div>
        </div>
      </div>
    );
  }
}

export default BrandCamera;
