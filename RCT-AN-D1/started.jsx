import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import NavBar from "./navbar";
import HomePage from "./homepage";
import Mobile from "./mobile";
import BrandMobile from "./brandMobile";
import Cart from "./cart";
import BrandLaptop from "./brandLaptop";
import Laptop from "./laptop";
import BrandCamera from "./brandCamera";
import Camera from "./camera";
class Started extends Component {
  state = {};
  render() {
    return (
      <div>
        <Route path="" component={NavBar} />
        <Switch>
          <Route path="/cart" component={Cart} />
          <Route path="/mobiles/:category/:id" component={Mobile} />
          <Route path="/camera/:category/:id" component={Camera} />
          <Route path="/laptop/:category/:id" component={Laptop} />
          <Route path="/laptop/:brand?" component={BrandLaptop} />
          <Route path="/camera/:brand?" component={BrandCamera} />
          <Route path="/mobile/:brand?" component={BrandMobile} />
          <Route path="/mobiles" component={HomePage} />
          <Redirect to="/mobiles" />
        </Switch>
      </div>
    );
  }
}

export default Started;
