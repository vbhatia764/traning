import React, { Component } from "react";
class Cart extends Component {
  state = {
    cart: JSON.parse(window.localStorage.getItem("cart"))
  };
  calcualteQuantity = () => {
    let { cart } = this.state;
    let q = 0;
    cart.map(n1 => (q = q + n1.qunatity));
    return q;
  };
  handleCalculatePrice = () => {
    let { cart } = this.state;
    let q = 0;
    cart.map(n1 => (q = q + n1.prod.price * n1.qunatity));
    return q;
  };
  handleQuantity = (item, sign) => {
    let { cart } = this.state;
    let index = cart.findIndex(n1 => n1 === item);
    cart[index].qunatity = cart[index].qunatity + sign;
    if (cart[index].qunatity === 0) {
      cart.splice(index, 1);
    }
    this.setState({ cart });
    window.localStorage.setItem("cart", JSON.stringify(cart));
    this.props.history.push("/cart");
  };
  render() {
    let { cart } = this.state;
    let quantity = this.calcualteQuantity();
    return (
      <div className="container-fluid" style={{ background: "lightgrey" }}>
        <div className="row mt-2">
          <div className="col-lg-8 col-12 bg-white ml-1 mr-1">
            <div className="row border-bottom">
              <div
                className="col-6"
                style={{
                  fontSize: "18px",
                  lineHeight: "56px",
                  padding: "0 24px",
                  fontWeight: "500"
                }}
              >
                My Cart({quantity})
              </div>
            </div>
            {cart.map(n1 => (
              <div>
                <div className="row mt-1 bg-white">
                  <div className="col-lg-2 col-4">
                    <div className="row">
                      <div className="col text-center">
                        <img
                          src={n1.prod.img}
                          style={{ width: "50px", height: "92px" }}
                        />
                      </div>
                    </div>
                    <div className="row mt-1">
                      <div className="col-12 text-center">
                        <button
                          className="btn btn-sm"
                          style={{
                            backgroundColor: "white",
                            borderColor: "#e0e0e0",
                            cursor: "pointer",
                            borderRadius: "50%",
                            fontSize: "12px"
                          }}
                          onClick={() => this.handleQuantity(n1, -1)}
                        >
                          -
                        </button>
                        <span
                          style={{
                            backgroundColor: "#fff",
                            border: "1px solid #d3d3d3",
                            color: "#000",
                            fontSize: "14px",
                            marginLeft: "2px",
                            marginRight: "2px",
                            paddingLeft: "10px",
                            paddingRight: "10px",
                            paddingBottom: "2px",
                            fontWeight: "400",
                            borderRadius: "3px"
                          }}
                        >
                          {n1.qunatity}
                        </span>
                        <button
                          className="btn btn-sm"
                          style={{
                            backgroundColor: "white",
                            borderColor: "#e0e0e0",
                            cursor: "auto",
                            borderRadius: "50%",
                            fontSize: "12px"
                          }}
                          onClick={() => this.handleQuantity(n1, 1)}
                        >
                          +
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-5 col-4">
                    <div className="row">
                      <div
                        className="col"
                        style={{ fontSize: "16px", color: "#212121" }}
                      >
                        {n1.prod.name}
                      </div>
                    </div>
                    <div className="row">
                      <div
                        className="col"
                        style={{ color: "#878787", fontSize: "14px" }}
                      >
                        {n1.prod.brand}
                      </div>
                    </div>
                    <div className="row mt-2">
                      <div className="col" style={{ fontWeight: "500" }}>
                        {" "}
                        &#8377;{n1.prod.price}
                        <span
                          style={{
                            textDecoration: "line-through",
                            fontSize: "16px",
                            color: "#878787"
                          }}
                        >
                          {" "}
                          &#8377;{n1.prod.prevPrice}{" "}
                        </span>{" "}
                        <span
                          style={{
                            color: "#388e3c",
                            fontSize: "16px",
                            fontWeight: "500"
                          }}
                        >
                          {" "}
                          {n1.prod.discount}%{" "}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 col-4">
                    <div className="row">
                      <div className="col" style={{ fontSize: "14px" }}>
                        Delivery in 2 days | Free{" "}
                        <span style={{ textDecoration: "line-through" }}>
                          {" "}
                          &#8377; 40
                        </span>
                      </div>
                    </div>
                    <div className="row">
                      <div
                        className="col"
                        style={{ fontSize: "12px", color: "grey" }}
                      >
                        {" "}
                        10 Days Replacement Policy{" "}
                      </div>
                    </div>
                  </div>
                </div>
                <hr />
              </div>
            ))}
          </div>
          <div className="col-lg-3 col-12 ml-1">
            <div className="row bg-white">
              <div className="col">
                <div
                  className="row border-bottom"
                  style={{
                    fontSize: "14px",
                    fontWeight: "500",
                    color: "#878787",
                    minHeight: "47px"
                  }}
                >
                  <div className="col pt-2">PRICE DETAILS</div>
                </div>
                <div className="row pt-2 pb-2">
                  <div className="col-6">Price({quantity} items)</div>
                  <div className="col-6 text-right">
                    &#8377;{this.handleCalculatePrice()}
                  </div>
                </div>
                <div className="row pt-2 pb-2">
                  <div className="col-6">Delivery</div>
                  <div className="col-6 text-success text-right">FREE</div>
                </div>
                <div
                  className="row pt-2 pb-2"
                  style={{
                    fontWeight: "500",
                    borderTop: "1px dashed e0e0e0",
                    fontSize: "18px"
                  }}
                >
                  <div className="col-6">Total Payable</div>
                  <div className="col-6 text-right">
                    &#8377;{this.handleCalculatePrice()}
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col-1">
                <img
                  src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/shield_435391.svg"
                  style={{ width: "25px" }}
                />
              </div>
              <div
                className="col-9"
                style={{
                  fontSize: "14px",
                  textAlign: "left",
                  fontWeight: "500",
                  display: "inline-block",
                  color: "#878787"
                }}
              >
                Safe and Secure Payments.Easy returns.100% Authentic products.
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Cart;
