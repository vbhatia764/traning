import React, { Component } from "react";
import "./dropbtn.css";
import "./cartcss.css";
import { Link } from "react-router-dom";
import { url } from "./config.json";
import http from "./httpService";
class NavBar extends Component {
  state = {
    cart: [],
    search: ""
  };
  componentDidMount() {
    let cart = JSON.parse(window.localStorage.getItem("cart"));
    if (cart && cart !== null) this.setState({ cart });
    else this.setState({ cart: [] });
  }
  componentDidUpdate(prevProps, prevState) {
    let path1 = prevProps.match;
    let path2 = this.props.match;
    if (path1 !== path2) {
      let cart = JSON.parse(window.localStorage.getItem("cart"));
      if (cart && cart !== null) this.setState({ cart });
      else this.setState({ cart: [] });
    }
  }
  handleCart = () => {
    this.props.history.push("/cart");
  };
  handleSubmit = async e => {
    e.preventDefault();
    let { search } = this.state;
    let endpoint = url + "/products/Mobiles?q=" + search;
    try {
      let { data } = await http.get(endpoint);
      console.log("data", data);
      let path = "/mobile?q=" + search;
      this.setState({ search: "" });
      this.props.history.push(path);
    } catch (error) {
      console.log(error);
    }
  };
  handleChange = e => {
    const { currentTarget: input } = e;
    let { search } = this.state;
    search = input.value;
    this.setState({ search });
  };

  handleCalculateQuantity = () => {
    let { cart } = this.state;
    let q = 0;
    cart.map(n1 => (q = q + n1.qunatity));
    return q;
  };
  render() {
    let { cart, search } = this.state;
    console.log("cart", cart);
    return (
      <div>
        <div className="row pb-2" style={{ backgroundColor: "#2874f0" }}>
          <div className="col-lg-2 col-4 mt-1 text-right">
            <div className="row ml-1">
              <Link to="/mobiles">
                <img
                  src="https://i.ibb.co/qs8BK6Y/flipkart-plus-4ee2f9.png"
                  className="img-fluid router-link-active"
                  style={{ width: "100px", cursor: "pointer" }}
                />
              </Link>
            </div>

            <div className="row text-primary ml-1">
              <a
                className="text-white"
                href="/mobiles"
                style={{ fontSize: "10px" }}
              >
                <i>
                  Explore
                  <span style={{ color: "yellow" }}>
                    Plus{" "}
                    <img
                      src="https://i.ibb.co/t2WXyzj/plus-b13a8b.png"
                      style={{ width: "10px" }}
                    />
                  </span>
                </i>
              </a>
            </div>
          </div>
          <div className="col-lg-4 col-5 mt-2 p-0">
            <form onSubmit={this.handleSubmit}>
              <input
                type="text"
                name="search"
                value={search}
                onChange={this.handleChange}
                className="form-control"
                placeholder="Search for products, brands and more"
              />
            </form>
          </div>
          <div
            className="col-2 text-white mt-1 text-center pt-2 d-none d-lg-block"
            style={{ minHeight: "20px" }}
          >
            <div className="dropdown">
              <div className="dropbtn1">
                My Account <span></span>
                <i
                  id="onhover"
                  className="fa fa-chevron-down"
                  style={{ fontSize: "10px", color: "lightgrey" }}
                ></i>
              </div>
              <div className="dropdown-content">
                <div>
                  <a href="#">My profile</a>
                </div>
                <div>
                  <a href="#">Orders</a>
                </div>
                <div>
                  <a href="#">WishList</a>
                </div>
              </div>
            </div>
          </div>
          <div
            className="col-1 text-white mt-1 pt-2 d-none d-lg-block"
            style={{ minHeight: "20px" }}
          >
            <div className="dropdown">
              <div className="dropbtn1">
                More <span></span>
                <i
                  id="onhover"
                  className="fa fa-chevron-down"
                  style={{ fontSize: "10px", color: "lightgrey" }}
                ></i>
              </div>
              <div className="dropdown-content">
                <div>
                  <a href="#">Notifications</a>
                </div>
                <div>
                  <a href="#">Sell on FlipKart</a>
                </div>
                <div>
                  <a href="#">24X7 Customer Care</a>
                </div>
                <div>
                  <a href="#">Advertise</a>
                </div>
              </div>
            </div>
          </div>
          <div
            className="col-lg-2 col-3 text-white mt-1 pt-2"
            style={{ cursor: "pointer", minHeight: "20px" }}
          >
            <i
              className="fa fa-shopping-cart"
              onClick={() => this.handleCart()}
            ></i>
            <span onClick={() => this.handleCart()}> Cart</span>

            {cart.length > 0 ? (
              <span id="cartcss">{this.handleCalculateQuantity()}</span>
            ) : (
              ""
            )}
          </div>
        </div>
        <div
          className="row bg-white"
          style={{
            color: "#212121",
            minHeight: "25px",
            fontFamily: "Roboto,Arial,sans-serif"
          }}
        >
          <div id="onhover" className="col-4 text-center">
            <div className="dropdown">
              <div className="dropbtn">
                Mobiles
                <i
                  id="onhover"
                  className="fa fa-chevron-down"
                  style={{ fontSize: "10px", color: "lightgrey" }}
                ></i>
                <div className="dropdown-content">
                  <div>
                    <Link to="/mobile/Mi">Mi</Link>
                  </div>
                  <div>
                    <Link to="/mobile/Realme">RealMe</Link>
                  </div>
                  <div>
                    <Link to="/mobile/Samsung">Samsung</Link>
                  </div>
                  <div>
                    <Link to="/mobile/OPPO">OPPO</Link>
                  </div>
                  <div>
                    <Link to="/mobile/Apple">Apple</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-4 text-center" id="onhover">
            <div className="dropdown">
              <div className="dropbtn">
                Laptops
                <i
                  id="onhover"
                  className="fa fa-chevron-down"
                  style={{ fontSize: "10px", color: "lightgrey" }}
                ></i>
                <div className="dropdown-content">
                  <div>
                    <Link to="/laptop/desktop">Desktop Pcs</Link>
                  </div>
                  <div>
                    <Link to="/laptop/gaming">Gaming Laptops</Link>
                  </div>
                  <div>
                    <Link to="/laptop/tablet">Tablets</Link>
                  </div>
                  <div>
                    <Link to="/laptop/accessories">Accessories</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-4 text-center">
            <div className="dropdown">
              <div className="dropbtn">
                Cameras
                <i
                  id="onhover"
                  className="fa fa-chevron-down"
                  style={{ fontSize: "10px", color: "lightgrey" }}
                ></i>
                <div className="dropdown-content">
                  <div>
                    <Link to="/camera/DSLR">DSLR</Link>
                  </div>
                  <div>
                    <Link to="/camera/lense">Lens</Link>
                  </div>
                  <div>
                    <Link to="/camera/tripod">Tripods</Link>
                  </div>
                  <div>
                    <Link to="/camera/compact">Compact</Link>
                  </div>
                  <div>
                    <Link to="/camera/accessories">Accessories</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NavBar;
