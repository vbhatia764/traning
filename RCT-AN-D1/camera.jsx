import React, { Component } from "react";
import { url2 } from "./config2.json";
import http from "./httpService";
class Camera extends Component {
  state = {
    data: [],
    selectedImage: "",
    cart: []
  };
  async componentDidMount() {
    let id = this.props.match.params.id;
    let category = this.props.match.params.category;
    let cart = JSON.parse(window.localStorage.getItem("cart"));
    if (cart && cart !== null) this.setState({ cart });
    let endpoint = url2 + "/camera/" + category + "/" + id;
    console.log("endpoint", endpoint);
    try {
      let { data } = await http.get(endpoint);
      let temp = {
        prod: data.prod,
        pics: data.pics,
        qunatity: 1
      };
      console.log(data);
      this.setState({ selectedImage: data.pics.imgList[0] });
      this.setState({ data: temp });
    } catch (error) {
      console.log(error);
    }
  }
  handleSelectedImage = image => {
    this.setState({ selectedImage: image });
  };
  handleAddToCart = () => {
    let id = this.props.match.params.id;
    let category = this.props.match.params.category;
    let search = this.props.location.search;
    search = search ? search : "";
    let { data, cart } = this.state;
    console.log(data);
    let index = cart.findIndex(n1 => n1.prod === data.prod);
    if (index === -1) cart.push(data);
    else cart[index].qunatity = cart[index].qunatity + 1;
    this.setState({ cart });
    console.log("cart", cart);
    window.localStorage.setItem("cart", JSON.stringify(cart));
    let endpoint = "/camera/" + category + "/" + id + search;
    this.props.history.push(endpoint);
  };
  handleBuyNow = () => {
    let { data, cart } = this.state;
    console.log(data);
    let index = cart.findIndex(n1 => n1.prod === data.prod);
    if (index === -1) cart.push(data);
    else cart[index].qunatity = cart[index].qunatity + 1;
    this.setState({ cart });
    console.log("cart", cart);
    window.localStorage.setItem("cart", JSON.stringify(cart));
    this.props.history.push("/cart");
  };
  render() {
    let { data, selectedImage } = this.state;
    return (
      <div className="container-fluid">
        <br />
        <div>
          <div className="row">
            <div className="col-lg-5 col-12">
              <div className="row p-0">
                <div className="col-lg-2 col-4 text-center">
                  {data.pics &&
                    data.pics.imgList.map((n1, i) => (
                      <div
                        key={i}
                        className={
                          n1 === selectedImage
                            ? "row border ml-lg-2 ml-0 border-primary"
                            : "row border ml-lg-2 ml-0 border-light"
                        }
                        style={{
                          height: "64px",
                          width: "64px",
                          textAlign: "center",
                          borderWidth: "2px solid"
                        }}
                      >
                        <div className="col text-center">
                          <img
                            src={n1}
                            style={{ width: "40px", height: "50px" }}
                            onClick={() => this.handleSelectedImage(n1)}
                          />
                        </div>
                      </div>
                    ))}
                </div>
                <div className="col-lg-8 col-8 border p-0 text-center">
                  <img src={selectedImage} className="img-fluid" />
                </div>
              </div>
              <div className="row">
                <div className="col-lg-2 col-4"></div>
                <div className="col-lg-4 col-4 text-sm-center">
                  <button
                    className="btn btn-sm btn-warning text-white"
                    onClick={() => this.handleAddToCart()}
                  >
                    <svg
                      class="_3oJBMI"
                      width="16"
                      height="16"
                      viewBox="0 0 16 15"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        className=""
                        d="M15.32 2.405H4.887C3 2.405
                                2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644
                                1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-
                                .003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-
                                1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-
                                1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86"
                        fill="#fff"
                      ></path>
                    </svg>
                    Add to Cart
                  </button>
                </div>
                <div className="col-lg-4 col-4">
                  <button
                    className="btn btn-sm text-white"
                    style={{ backgroundColor: "#fb641b" }}
                    onClick={() => this.handleBuyNow()}
                  >
                    <i className="fa fa-bolt"></i>
                    Buy Now
                  </button>
                </div>
              </div>
            </div>
            <div className="col-lg-7 col-12">
              <div className="row"></div>
              <div
                className="row"
                style={{
                  color: "#212121",
                  fontSize: "18px",
                  fontWeight: "inherit",
                  display: "inline-block"
                }}
              >
                <div className="col">{data.prod && data.prod.name}</div>
              </div>
              <div className="row">
                <div className="col">
                  <span
                    style={{
                      lineHeight: "normal",
                      display: "inline-block",
                      color: "white",
                      verticalAlign: "middle",
                      backgroundColor: "#388e3c"
                    }}
                    className="rounded"
                  >
                    <strong>
                      {data.prod && data.prod.rating}
                      <img
                        src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwM
C9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQ
zOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEyb
DEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                      />
                    </strong>
                  </span>{" "}
                  <span
                    className="text-muted"
                    style={{ fontSize: "14px", fontWeight: "500" }}
                  >
                    {data.prod && data.prod.ratingDesc}
                  </span>{" "}
                  <span>
                    <img
                      className="img-fluid"
                      src="https://i.ibb.co/t8bPSBN/fa-8b4b59.png"
                      style={{ width: "70px" }}
                    />
                  </span>
                </div>
              </div>
              <div className="row mt-2">
                <div
                  className="col"
                  style={{
                    fontSize: "28px",
                    verticalAlign: "sub",
                    fontWeight: "500"
                  }}
                >
                  &#8377;{data.prod && data.prod.price}{" "}
                  <span
                    style={{
                      textDecoration: "line-through",
                      fontSize: "16px",
                      color: "#878787"
                    }}
                  >
                    {data.prod && data.prod.prevPrice}
                  </span>
                  {"  "}
                  <span
                    style={{
                      color: "#388e3c",
                      fontSize: "16px",
                      fontWeight: "500"
                    }}
                  >
                    {data.prod && data.prod.discount}%
                  </span>
                </div>
              </div>
              <div className="row">
                <div
                  className="col"
                  style={{
                    fontSize: "16px",
                    marginLeft: "12px",
                    verticalAlign: "middle",
                    fontWeight: "500",
                    color: "black"
                  }}
                >
                  Available Offers
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <img
                    style={{ height: "18px", width: "18px" }}
                    src="https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png"
                  />
                  <span
                    style={{
                      color: "#212121",
                      fontWeight: "500",
                      paddingRight: "4px",
                      fontSize: "14px"
                    }}
                  >
                    Bank Offer
                  </span>{" "}
                  <span style={{ fontSize: "14px" }}>
                    10% Cashback* on HDFC Bank Debit Cards
                  </span>
                </div>
                <div className="col-12">
                  <img
                    style={{ height: "18px", width: "18px" }}
                    src="https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png"
                  />
                  <span
                    style={{
                      color: "#212121",
                      fontWeight: "500",
                      paddingRight: "4px",
                      fontSize: "14px"
                    }}
                  >
                    Bank Offer
                  </span>{" "}
                  <span style={{ fontSize: "14px" }}>
                    5% Unlimited Cashback on Flipkart Axis Bank Credit Card
                  </span>
                </div>
                <div className="col-12">
                  <img
                    style={{ height: "18px", width: "18px" }}
                    src="https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png"
                  />
                  <span
                    style={{
                      color: "#212121",
                      fontWeight: "500",
                      paddingRight: "4px",
                      fontSize: "14px"
                    }}
                  >
                    Bank Offer
                  </span>{" "}
                  <span style={{ fontSize: "14px" }}>
                    5% Instant Discount on EMI with ICICI Bank Credit and Debit
                    Cards
                  </span>
                </div>
                <div className="col-12">
                  <img
                    style={{ height: "18px", width: "18px" }}
                    src="https://i.ibb.co/PgZY1JF/f3bae257-60c1-4ef5-960a-1d8170ea7a42.png"
                  />
                  <span
                    style={{
                      color: "#212121",
                      fontWeight: "500",
                      paddingRight: "4px",
                      fontSize: "14px"
                    }}
                  >
                    Bank Offer
                  </span>{" "}
                  <span style={{ fontSize: "14px" }}>
                    Get upto ₹10800 off on exchange
                  </span>
                </div>
                <div className="col-12">
                  <img
                    style={{ height: "18px", width: "18px" }}
                    src="https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png"
                  />
                  <span
                    style={{
                      color: "#212121",
                      fontWeight: "500",
                      paddingRight: "4px",
                      fontSize: "14px"
                    }}
                  >
                    Bank Offer
                  </span>{" "}
                  <span style={{ fontSize: "14px" }}>Special Price</span>
                </div>
                <div className="col-12">
                  <img
                    style={{ height: "18px", width: "18px" }}
                    src="https://i.ibb.co/zVRyyTn/49f16fff-0a9d-48bf-a6e6-5980c9852f11.png"
                  />
                  <span
                    style={{
                      color: "#212121",
                      fontWeight: "500",
                      paddingRight: "4px",
                      fontSize: "14px"
                    }}
                  >
                    Bank Offer
                  </span>{" "}
                  <span style={{ fontSize: "14px" }}>No Cost EMI</span>
                </div>
              </div>

              <div className="row mt-2">
                <div
                  className={
                    data.pics && data.pics.brandImg !== ""
                      ? "col-lg-1 col-3 border text-center"
                      : "col-lg-1 col-3 text-center"
                  }
                >
                  {data.pics && data.pics.brandImg !== "" ? (
                    <img
                      src={data.pics && data.pics.brandImg}
                      style={{ width: "38px", height: "30px" }}
                    />
                  ) : (
                    ""
                  )}
                </div>
                <div
                  className="col-8 ml-3 d-none d-lg-block"
                  style={{ fontSize: "14px" }}
                >
                  Brand Warranty of 1 Year Available for Mobile and 6 Months for
                  Accessories
                </div>
              </div>
              <div className="row mt-2">
                <div
                  className="col-1 d-none d-lg-block"
                  style={{
                    fontWeight: "500",
                    color: "#878787",
                    width: "110px",
                    paddingRight: "10px",
                    float: "left",
                    fontSize: "14px"
                  }}
                >
                  Highlights
                </div>
                <div className="col-5 d-none d-lg-block">
                  <ul>
                    {data.prod &&
                      data.prod.details.map(n1 => (
                        <li
                          key={n1}
                          style={{
                            fontSize: "14px",
                            color: "grey",
                            lineHeight: "1.4"
                          }}
                        >
                          <span style={{ color: "black" }}>{n1}</span>
                        </li>
                      ))}
                  </ul>
                </div>
                <div
                  className="col-2 d-none d-lg-block"
                  style={{
                    fontWeight: "500",
                    color: "#878787",
                    width: "110px",
                    paddingRight: "10px",
                    float: "left",
                    fontSize: "14px"
                  }}
                >
                  Easy Payment Options
                </div>
                <div className="col-4 d-none d-lg-block">
                  <ul
                    style={{
                      fontSize: "14px",
                      color: "grey",
                      lineHeight: "1.4"
                    }}
                  >
                    <li>
                      <span style={{ color: "black" }}>
                        {" "}
                        No cost EMI NaN/month
                      </span>
                    </li>
                    <li>
                      <span style={{ color: "black" }}>
                        Debit/Flipkart EMI available
                      </span>
                    </li>
                    <li>
                      <span style={{ color: "black" }}>Cash on Delivery</span>
                    </li>
                    <li>
                      <span style={{ color: "black" }}>
                        Net Banking & Credit/Debit/ATM Card
                      </span>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="row">
                <div
                  className="col-lg-1 col-3"
                  style={{
                    fontWeight: "500",
                    color: "#878787",
                    width: "110px",
                    paddingRight: "10px",
                    float: "left",
                    fontSize: "14px"
                  }}
                >
                  Seller
                </div>
                <div className="col-lg-9 col-9">
                  <span
                    style={{
                      color: "#2874f0",
                      fontSize: "14px",
                      fontWeight: "500"
                    }}
                  >
                    SuperComNet
                  </span>{" "}
                  <span
                    style={{
                      lineHeight: "normal",
                      display: "inline-block",
                      color: "#fff",
                      fontSize: "12px",
                      borderRadius: "4px",
                      backgroundColor: "#2874f0"
                    }}
                  >
                    4.2
                    <img
                      src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwM
C9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQ
zOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEyb
DEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                    />
                  </span>
                  <ul className="d-none d-lg-block">
                    <li
                      style={{
                        fontSize: "14px",
                        color: "grey",
                        lineHeight: "1.4"
                      }}
                    >
                      10 Day replacement
                    </li>
                  </ul>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <img
                    className="img-fluid"
                    src="https://i.ibb.co/j8CMRbn/CCO-PP-2019-07-14.png"
                    style={{ width: "300px", height: "85px" }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Camera;
