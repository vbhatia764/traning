import React, { Component } from "react";
import { getCurrentUser } from "./currentUser";
import { Link } from "react-router-dom";

class NavBar extends Component {
  state = {};
  render() {
    let user = getCurrentUser();
    return (
      <div>
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
          {user && (
            <Link className="navbar-brand" to="#">
              BookSite
            </Link>
          )}

          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link className="nav-link" to="/books?newarrival=yes">
                  New Arrivals
                </Link>
              </li>

              <li className="nav-item dropdown">
                <Link
                  className="nav-link dropdown-toggle"
                  to="#"
                  id="navbarDropdown"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Book By Genre
                </Link>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <Link className="dropdown-item" to="/books/Fiction">
                    Fiction
                  </Link>
                  <Link className="dropdown-item" to="/books/Children">
                    Children
                  </Link>
                  <Link className="dropdown-item" to="/books/Mystery">
                    Mystery
                  </Link>
                  <Link className="dropdown-item" to="/books/Management">
                    Management
                  </Link>
                  <Link className="dropdown-item" to="/books/Self Help">
                    Self help
                  </Link>
                </div>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/books">
                  All Books
                </Link>
              </li>
            </ul>

            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                {user && (
                  <Link className="nav-link text-primary" to="#">
                    Welcome {user && user.name}
                  </Link>
                )}
              </li>
              <li className="nav-item">
                {user ? (
                  <Link className="nav-link" to="/userFav">
                    Favorites
                  </Link>
                ) : (
                  <Link className="nav-link" to="/login">
                    Favorites
                  </Link>
                )}
              </li>
              <li className="nav-item">
                {!user ? (
                  <a className="nav-link" href="/login">
                    LOGIN
                  </a>
                ) : (
                  <a className="nav-link" href="/logout">
                    Logout
                  </a>
                )}
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default NavBar;
