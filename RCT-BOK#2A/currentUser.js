export function getCurrentUser() {
  const user = JSON.parse(localStorage.getItem("token"));
  return user;
}
export default {
  getCurrentUser
};
