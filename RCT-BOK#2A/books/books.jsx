import React, { Component } from "react";
import { Link } from "react-router-dom";
import queryString from "query-string";
import { url } from "../config.json";
import axios from "axios";
import httpService from "../../services/httpService.js";
import LeftPanel from "./leftPanel";
import Detail from "./../favourite/detail";

class Books extends Component {
  state = {
    selopt: this.props.match.params.selopt,
    data: [],
    details: {},
    pageNumber: "",
    maxPages: 0,
    noOfItems: 0,
    total: 0,
    path: "",
    bestsellers: [
      { totalNum: "74", refineValue: "No" },
      { totalNum: "73", refineValue: "Yes" }
    ],
    languages: [
      { totalNum: "83", refineValue: "English" },
      { totalNum: "16", refineValue: "French" },
      { totalNum: "21", refineValue: "Latin" },
      { totalNum: "27", refineValue: "Other" }
    ],
    currentPosition: 0,
    count: 1
  };

  makeCbStructure(option, value) {
    let display = "";
    if (
      value === "No" ||
      value === "Yes" ||
      value === "Yes,No" ||
      value === "No,Yes"
    )
      display = "Bestseller";
    else display = "Language";
    let temp = option.map(n1 => ({
      totalNum: n1.totalNum,
      refineValue: n1.refineValue,
      isSelected: false
    }));
    let temp2 = {
      options: temp,
      display: display,
      selected: ""
    };
    let cnames = value;

    let obj = temp2.options.find(n1 => n1.refineValue === cnames);
    if (obj) {
      obj.isSelected = true;
      temp2.selected = obj.refineValue;
    }

    return temp2;
  }
  async componentDidMount() {
    let endponit = url + "/booksapp/books";
    let { data } = await httpService.get(endponit);
    this.setState({ data: data.data });
    this.setState({ pageNumber: data.pageInfo.pageNumber });
    this.setState({ maxPages: data.pageInfo.numberOfPages });
    this.setState({ noOfItems: data.pageInfo.numOfItems });
    this.setState({ total: data.pageInfo.totalItemCount });
  }
  async componentDidUpdate(prevProps, prevState) {
    let path1 = prevProps.match;
    let path2 = this.props.match;

    let { bestSeller, language } = queryString.parse(
      this.props.location.search
    );
    bestSeller = bestSeller ? bestSeller : "";
    language = language ? language : "";

    let data;

    let nw = this.props.location.search;

    let url1;

    if (path1 !== path2) {
      if (nw === "") url1 = "http://localhost:2410/booksapp" + path2.url;
      else url1 = "http://localhost:2410/booksapp" + path1.url + nw;

      if (this.props.location.pathname === "/books?newarrival=yes") {
        let search = this.props.location.search;

        let temp = search.slice(1);
        url1 =
          "http://localhost:2410/booksapp" +
          this.props.location.pathname +
          "&" +
          temp;
      } //newArrival if

      let search = this.props.location.search;

      if (bestSeller && !language) {
        url1 = "http://localhost:2410/booksapp/books" + search;
      }
      if (language && !bestSeller) {
        url1 = "http://localhost:2410/booksapp/books" + search;
      } else if (bestSeller && language) {
        url1 = "http://localhost:2410/booksapp/books" + search;
      }

      const { data } = await axios.get(url1);

      this.setState({ data: data.data });

      this.setState({ pageNumber: data.pageInfo.pageNumber });
      this.setState({ maxPages: data.pageInfo.numberOfPages });
      this.setState({ noOfItems: data.pageInfo.numOfItems });
      this.setState({ total: data.pageInfo.totalItemCount });
    } //main if
  }

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";

      params = params + newParamName + "=" + newParamValue;
    }

    return params;
  }

  handleOptionChange = (languageCheckBoxes, bestSellerCheckBoxes) => {
    console.log("lang", languageCheckBoxes);
    console.log("best", bestSellerCheckBoxes);
    let temp1 = languageCheckBoxes.options.find(n1 => n1.isSelected);

    let language = temp1 === undefined ? "" : temp1.refineValue;

    let temp2 = bestSellerCheckBoxes.options.find(n1 => n1.isSelected);

    let bestSeller = temp2 === undefined ? "" : temp2.refineValue;

    if (
      temp1 !== undefined &&
      temp1 !== "" &&
      temp2 !== undefined &&
      temp2 !== ""
    ) {
      let f1 = this.state.bestsellers;
      temp1.refineValue = this.state.maxPages;
      temp2.refineValue = this.state.maxPages;
    }
    this.setState({ count: 1 });
    this.callUrl("", language, bestSeller);
  };

  callUrl = (params, language, bestSeller) => {
    let path = "/books";
    const selopt = this.props.match.params.selopt;
    const search = this.props.location.search;
    const { newarrival } = queryString.parse(this.props.location.search);
    let new2 = this.props.location.pathname;

    let { page } = queryString.parse(this.props.location.search);

    if (selopt) {
      path = path + "/" + selopt;
    }

    if (
      search === "?newarrival=yes" ||
      this.props.location.pathname === "/books?newarrival=yes"
    ) {
      path = path + "?newarrival=yes";
    } else {
      this.setState({ path: "" });
    }

    if (path === "/books?newarrival=yes") this.setState({ path });

    if (params === "") {
      params = this.addToParams(params, "language", language);
      params = this.addToParams(params, "bestSeller", bestSeller);
    }

    this.setState({ path: path });
    this.props.history.push({
      pathname: path,
      search: params
    });
  };
  gotoPage = x => {
    let { page, bestSeller, language } = queryString.parse(
      this.props.location.search
    );
    let { count } = this.state;
    let currPage = page ? +page : 1;
    currPage = currPage + x;
    bestSeller = bestSeller ? bestSeller : "";
    language = language ? language : "";

    let params = "";
    if (x === -1) count = count - 10;
    else if (x === 1) count = count + 10;
    this.setState({ count });
    params = this.addToParams(params, "language", language);
    params = this.addToParams(params, "bestSeller", bestSeller);
    params = this.addToParams(params, "page", currPage);

    this.callUrl(params, language, bestSeller);
  };
  getBack = () => {
    this.setState({ currentPosition: 0 });
    this.setState({ details: {} });
    this.setState({ count: 1 });
  };
  showDetails = details => {
    this.setState({ details });
    this.setState({ currentPosition: 1 });
  };
  render() {
    let { page, bestSeller, language } = queryString.parse(
      this.props.location.search
    );
    let {
      details,
      currentPosition,
      bestsellers,
      languages,
      data,
      count
    } = this.state;
    page = page ? page : 1;
    bestSeller = bestSeller ? bestSeller : "";
    language = language ? language : "";

    if (bestSeller) {
      bestsellers = bestsellers.filter(n1 => n1.refineValue === bestSeller);
    }
    if (language) {
      languages = languages.filter(n1 => n1.refineValue === language);
    }

    let bestSellerCheckBoxes = this.makeCbStructure(bestsellers, bestSeller);
    let languageCheckBoxes = this.makeCbStructure(languages, language);
    return (
      <div>
        <div className="container">
          {currentPosition === 0 ? (
            <div>
              <div className="row">
                <div className="col-12 col-md-2 col-lg-2">
                  <div className="row navbar-expand-md">
                    <div className="col text-right">
                      <button
                        className="btn navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarsupportedContent"
                        style={{ background: "#249cd4" }}
                      >
                        OPTIONS
                      </button>
                    </div>
                  </div>
                  <nav className="navbar navbar-expand-md ">
                    {/*<div className="float-left">
                      <button
                        className="btn navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarsupportedContent"
                        style={{ background: "#249cd4" }}
                      >
                        OPTIONS
                      </button>
          </div>*/}
                    <div
                      className="collapse navbar-collapse"
                      id="navbarsupportedContent"
                    >
                      <LeftPanel
                        bestSellerCheckBoxes={bestSellerCheckBoxes}
                        languageCheckBoxes={languageCheckBoxes}
                        key={languageCheckBoxes}
                        onOptionChange={this.handleOptionChange}
                      />
                    </div>
                  </nav>
                </div>
                <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9 text-left">
                  {count} to
                  {data && count + data.length - 1} of
                  {this.state.total}
                  <div
                    className="row border text-center"
                    style={{ background: "blue" }}
                  >
                    <div className="d-block col-6 col-sm-4 col-md-3 col-lg-3 border">
                      Title
                    </div>
                    <div className="d-none d-md-block col-md-3 col-lg-3 border">
                      Author
                    </div>
                    <div className="d-none d-sm-block col-md-3 col-sm-4 col-lg-2 border">
                      Language
                    </div>
                    <div className="d-none d-lg-block col-lg-2 border">
                      Genre
                    </div>
                    <div className="d-block col-6 col-sm-4 col-md-3 col-lg-1 border">
                      Price
                    </div>
                    <div className="d-none d-lg-block col-lg-1 border">
                      Best Seller
                    </div>
                    {/*row */}
                  </div>
                  {this.state.data.map(n1 => (
                    <div className="row border text-center" key={n1.name}>
                      <div className="d-block col-6 col-sm-4 col-md-3 col-lg-3 border">
                        <Link to="/books" onClick={() => this.showDetails(n1)}>
                          {n1.name}
                        </Link>
                      </div>
                      <div className="d-none d-md-block col-md-3 col-lg-3 border">
                        {n1.author}
                      </div>
                      <div className="d-none d-sm-block col-md-3 col-sm-4 col-lg-2 border">
                        {n1.language}
                      </div>
                      <div className="d-none d-lg-block col-lg-2 border">
                        {n1.genre}
                      </div>
                      <div className="d-block col-6 col-sm-4 col-md-3 col-lg-1 border">
                        {n1.price}
                      </div>
                      <div className="d-none d-lg-block col-lg-1 border">
                        {n1.bestseller}
                      </div>
                    </div>
                  ))}
                  {/*map data */}
                  <div className="row">
                    {page > 1 ? (
                      <div className="text-left">
                        <button
                          className="btn btn-primary m-2"
                          onClick={() => this.gotoPage(-1)}
                        >
                          Previous
                        </button>
                      </div>
                    ) : (
                      ""
                    )}

                    {page < this.state.maxPages ? (
                      <div className="text-right">
                        <button
                          className="btn btn-primary m-2"
                          onClick={() => this.gotoPage(1)}
                        >
                          Next
                        </button>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
                {/*row */}
              </div>
            </div>
          ) : (
            <Detail details={details} onAdd={this.getBack} />
          )}
        </div>
      </div>
    );
  }
}

export default Books;
