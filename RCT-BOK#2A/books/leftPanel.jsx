import React, { Component } from "react";
class LeftPanel extends Component {
  state = {
    languageCheckBoxes: [],
    bestSellerCheckBoxes: []
  };
  handleSubmit = () => {
    let { languageCheckBoxes, bestSellerCheckBoxes } = this.state;
    console.log(languageCheckBoxes);
    console.log(bestSellerCheckBoxes);
    this.props.onOptionChange(languageCheckBoxes, bestSellerCheckBoxes);
  };
  handleChange = e => {
    const { currentTarget: input } = e;
    const { languageCheckBoxes, bestSellerCheckBoxes } = this.props;
    if (input.name === "Yes" || input.name === "No") {
      if (input.type === "checkbox") {
        let cb = bestSellerCheckBoxes.options.find(
          n1 => n1.refineValue === input.name
        );
        if (cb) cb.isSelected = input.checked;
      }
    } else {
      if (input.type === "checkbox") {
        let cb = languageCheckBoxes.options.find(
          n1 => n1.refineValue === input.name
        );
        if (cb) cb.isSelected = input.checked;
      }
    }

    this.setState({ languageCheckBoxes });
    this.setState({ bestSellerCheckBoxes });
  };
  render() {
    let { languageCheckBoxes, bestSellerCheckBoxes } = this.props;
    return (
      <div>
        <div className="row border" style={{ height: "50px" }}>
          <h6 className="font-weight-bold col text-left">Options</h6>
        </div>
        <form>
          <div className="row border">
            <h6 className="col text-left font-weight-bold">Bestseller</h6>

            {bestSellerCheckBoxes.options.map((item, index) => (
              <div
                className="col-12 text-left"
                key={index}
                style={{ height: "40px" }}
              >
                <div className="form-check" key={item.refineValue}>
                  <input
                    type="checkbox"
                    name={item.refineValue}
                    value={item.isSelected}
                    checked={item.isSelected}
                    onChange={this.handleChange}
                    className="form-check-input"
                  />
                  <label className="form-check-label">
                    {item.refineValue}({item.totalNum})
                  </label>
                </div>
              </div>
            ))}
          </div>
          <div className="row border">
            <h6 className="col-12 text-left font-weight-bold">Language</h6>

            {languageCheckBoxes.options.map((item, index) => (
              <div
                className="col-12 text-left"
                key={index}
                style={{ height: "40px" }}
              >
                <div className="form-check" key={item.refineValue}>
                  <input
                    type="checkbox"
                    name={item.refineValue}
                    value={item.isSelected}
                    checked={item.isSelected}
                    onChange={this.handleChange}
                    className="form-check-input"
                  />
                  <label className="form-check-label">
                    {item.refineValue}({item.totalNum})
                  </label>
                </div>
              </div>
            ))}
          </div>
        </form>
        <div className="row border" style={{ height: "50px" }}>
          <h6 className="col-12 m-1 text-left">
            <button onClick={() => this.handleSubmit()}>Refine</button>
          </h6>
        </div>
      </div>
    );
  }
}

export default LeftPanel;
