import React, { Component } from "react";
import { url } from "../config.json";
import { Link } from "react-router-dom";
import httpService from "../../services/httpService.js";
import queryString from "query-string";
import Detail from "./detail";
class Favourite extends Component {
  state = { data: [], count: 1, currentPosition: 0, details: {} };
  async componentDidMount() {
    let endpoint = url + "/booksapp/books";
    try {
      let { data } = await httpService.get(endpoint);

      this.setState({ data: data.data });
    } catch (error) {
      console.log(error);
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let path1 = prevProps.match;
    let path2 = this.props.match;
    let search = this.props.location.search;

    if (path1 !== path2) {
      let endpoint = url + "/booksapp/books" + search;
      try {
        let { data } = await httpService.get(endpoint);

        this.setState({ data: data.data });
      } catch (error) {
        console.log(error);
      }
    }
  }
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";

      params = params + newParamName + "=" + newParamValue;
    }

    return params;
  }
  gotoPage = x => {
    let { page } = queryString.parse(this.props.location.search);
    let { count } = this.state;
    let currPage = page ? +page : 1;
    currPage = currPage + x;

    let params = "";
    if (x === -1) count = count - 10;
    else if (x === 1) count = count + 10;
    this.setState({ count });
    params = this.addToParams(params, "page", currPage);

    let path = "/favourite";
    this.props.history.push({
      pathname: path,
      search: params
    });
  };
  getBack = () => {
    this.setState({ currentPosition: 0 });
    this.setState({ details: {} });
  };
  showDetails = details => {
    console.log("details", details);
    this.setState({ details });
    this.setState({ currentPosition: 1 });
  };
  render() {
    let { data, count, currentPosition, details } = this.state;
    let { page } = queryString.parse(this.props.location.search);
    page = page ? page : 1;
    return (
      <div className="container">
            <div className="row text-center" style={{ background: "#338fc4" }}>
              <div className="col border">Title</div>
              <div className="col border">Author</div>
            </div>
            {data &&
              data.map((n1, index) => (
                <div className="row text-center" key={index}>
                  <div className="col border">
                    <Link to="/favourite" onClick={() => this.showDetails(n1)}>
                      {n1.name}
                    </Link>
                  </div>
                  <div className="col border">{n1.author}</div>
                </div>
              ))}
        
        
      </div>
    );
  }
}
export default Favourite;
