import React, { Component } from "react";
import { url } from "../config.json";
import httpService from "../../services/httpService.js";
import { Link } from "react-router-dom";
import { getCurrentUser } from "./../currentUser";
class Detail extends Component {
  state = {};
  addToFav = async id => {
    let user = getCurrentUser();
    let endpoint = url + "/booksapp/favorites/" + user.userid;
    let data = { bookid: id, userid: user.userid };
    try {
      console.log("data", data);
      let response = await httpService.post(endpoint, data);
      console.log("response", response);
      alert("book added to favourites");
      this.props.onAdd();
    } catch (error) {
      console.log(error);
    }
  };
  render() {
    let user = getCurrentUser();
    let { details } = this.props;
    console.log("details props", details);
    return (
      <div className="container">
        <h2>BOOK : {details.name}</h2>
        <div className="row border">
          <div className="col">Author :</div>
          <div className="col">{details.author}</div>
        </div>
        <div className="row border">
          <div className="col">Genre :</div>
          <div className="col">{details.genre}</div>
        </div>
        <div className="row border">
          <div className="col">Publisher :</div>
          <div className="col">{details.publisher}</div>
        </div>
        <div className="row border">
          <div className="col">Description :</div>
          <div className="col">{details.description}</div>
        </div>
        <div className="row border">
          <div className="col">Blurb :</div>
          <div className="col">{details.blurb}</div>
        </div>
        <div className="row border">
          <div className="col">Review :</div>
          <div className="col">{details.review}</div>
        </div>
        <div className="row border">
          <div className="col">Price :</div>
          <div className="col">{details.price}</div>
        </div>
        <div className="row border">
          <div className="col">Review :</div>
          <div className="col">{details.avgrating}</div>
        </div>
        {user && (
          <div className="row border">
            <div className="col">
              <Link to="/books" onClick={() => this.addToFav(details.bookid)}>
                Add to favourites
              </Link>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default Detail;
