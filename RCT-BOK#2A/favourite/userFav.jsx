import React, { Component } from "react";
import { url } from "../config.json";
import httpService from "../../services/httpService.js";
import { Link } from "react-router-dom";
import { getCurrentUser } from "./../currentUser";
import Pagination from "./pagination";
import { paginate } from "./paginate";
class UserFav extends Component {
  state = {
    data: [],
    pageSize: 10,
    currentPage: 1,
    count: 1
  };
  async componentDidMount() {
    let user = getCurrentUser();
    let endpoint = url + "/booksapp/favorites/" + user.userid;
    console.log("endpoint", endpoint);
    let { data } = await httpService.get(endpoint);
    let res = await httpService.get(endpoint);
    console.log("data", res);
    this.setState({ data });
  }
  handlePageChange = page => {
    let currentPage = this.state.currentPage;
    let count = this.state.count;
    currentPage = currentPage + page;
    if (page === -1) count = count - 10;
    else if (page === 1) count = count + 10;
    this.setState({ count });
    this.setState({ currentPage });
  };
  render() {
    let { data: allData, pageSize, count, currentPage } = this.state;
    const data = paginate(allData, currentPage, pageSize);
    console.log("data", data);

    return (
      <div className="container">
        {data.length === 0 ? (
          <h4>No Books added so far</h4>
        ) : (
          <div>
            <h5>
              Favourites {count}-{count + data.length - 1} of {allData.length}
            </h5>
            <div className="row text-center" style={{ background: "#338fc4" }}>
              <div className="col border">Title</div>
              <div className="col border">Author</div>
            </div>
            {data &&
              data.map((n1, index) => (
                <div className="row text-center" key={index}>
                  <div className="col border text-primary">{n1.name}</div>
                  <div className="col border">{n1.author}</div>
                </div>
              ))}
          </div>
        )}
        <div className="row m-1">
          {currentPage > 1 ? (
            <div className="col">
              <button
                className="btn btn-primary"
                onClick={() => this.handlePageChange(-1)}
              >
                Prev
              </button>
            </div>
          ) : (
            ""
          )}
          {count + data.length - 1 !== allData.length ? (
            <div className="col">
              <button
                className="btn btn-primary"
                onClick={() => this.handlePageChange(1)}
              >
                Next
              </button>
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}

export default UserFav;
