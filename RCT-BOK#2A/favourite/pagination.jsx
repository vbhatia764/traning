import React, { Component } from "react";
import _ from "lodash";
const Pagination = props => {
  const { itemsCount, pageSize, onPageChange } = props;
  const pageCount = Math.ceil(itemsCount / pageSize);
  if (pageCount === 1) return null;
  const pages = _.range(1, pageCount + 1);
  console.log("pageCount", pages);

  return (
    <nav>
      <ul class="pagination">
        <li class="page-item">
          <a class="page-link m-1" onClick={() => onPageChange(-1)}>
            Previous
          </a>
        </li>
        <li class="page-item">
          <a class="page-link m-1" onClick={() => onPageChange(+1)}>
            Next
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default Pagination;
