import React, { Component } from "react";
import NavBar from "./navbar";
import Login from "./login";
import Register from "./register";
import { Route, Redirect, Switch } from "react-router-dom";
import Books from "./books/books";
import Logout from "./logout";
import ProtectatedRoute from "./protectatedRoute";
import AllFavourite from "./favourite/allFavourite";
import UserFav from "./favourite/userFav";
class Started extends Component {
  state = {};
  render() {
    return (
      <div>
        <NavBar />
        <Switch>
          <ProtectatedRoute path="/userFav" component={UserFav} />
          <Route path="/books?newarrival=yes" component={Books} />
          <Route path="/books/:selopt?" component={Books} />
          <Route path="/login" component={Login} />
          <Route path="/logout" component={Logout} />
          <Route path="/register" component={Register} />
          <Redirect to="/login" />
        </Switch>
      </div>
    );
  }
}

export default Started;
