import React, { Component } from "react";
import { Route } from "react-router-dom";
import { Redirect } from "react-router-dom";
import { getCurrentUser } from "./currentUser";
const ProtectatedRoute = ({ path, component: Component, render, ...rest }) => {
  const user = getCurrentUser();
  console.log(user);
  return (
    <Route
      {...rest}
      render={props => {
        if (!user) return <Redirect to="/login" />;
        return Component ? <Component {...props} /> : render(props);
      }}
    />
  );
};

export default ProtectatedRoute;
