import React, { Component } from "react";
import { Link } from "react-router-dom";
import { url } from "./config.json";
import http from "../services/httpService";
class Register extends Component {
  state = { login: { name: "", password: "", repassword: "" }, errors: {} };
  handleSubmit = async e => {
    e.preventDefault();
    let error = this.validate();
    this.setState({ errors: error });
    let errorCount = Object.keys(error).length;

    if (errorCount > 0) return;
    let login = { ...this.state.login };
    let data = {
      name: login.name,
      password: login.password
    };
    let endpoint = url + "/booksapp/newuser";
    try {
      let response = await http.post(endpoint, data);
      console.log("response", response);
      window.location = "/login";
    } catch (error) {
      console.log("error", error);
      let errors = { ...this.state.errors };
      errors.data = "something went wrong";
      this.setState({ errors });
    }
  };
  validate = () => {
    let error = {};
    if (!this.state.login.name.trim()) error.name = "username is required";
    else if (this.state.login.name.trim().length < 6)
      error.name = "Username should be minimum 6 character long";
    if (!this.state.login.password.trim())
      error.password = "password is required";
    else if (this.state.login.password.trim().length < 6)
      error.password = "Password should be minimum 6 character long";

    if (this.state.login.repassword.trim() !== this.state.login.password.trim())
      error.repassword = "password do not match";

    return error;
  };
  handleChange = e => {
    let errString = this.validateInput(e);
    console.log(errString);
    const error = { ...this.state.errors };
    error[e.currentTarget.name] = errString;
    const { currentTarget: input } = e;
    let login = { ...this.state.login };
    login[input.name] = input.value;
    this.setState({ login });
    this.setState({ errors: error });
  };
  validateInput = e => {
    switch (e.currentTarget.name) {
      case "name":
        if (!e.currentTarget.value.trim()) return "name is required";
        if (e.currentTarget.value.trim().length < 6)
          return "Username should be minimum 6 character long";
        break;

      case "password":
        if (!e.currentTarget.value.trim()) return "password is required";
        else if (e.currentTarget.value.trim().length < 6)
          return "Password should be minimum 6 character long";
        break;

      default:
        break;
    }
    return "";
  };
  render() {
    let { login, errors } = this.state;
    return (
      <div className="bg-light">
        <div className="row">
          <div className="col-4"></div>
          <div className="col-4">
            <form onSubmit={this.handleSubmit}>
              <h3>Register</h3>
              {errors.data ? (
                <div className="text-danger">{errors.data}</div>
              ) : (
                ""
              )}
              <div className="form-group">
                <label htmlFor="name">Username</label>
                <input
                  className="text"
                  className="form-control"
                  id="name"
                  value={login.name}
                  name="name"
                  onChange={this.handleChange}
                />
                {errors.name ? (
                  <div className="text-danger">{errors.name}</div>
                ) : (
                  ""
                )}
              </div>

              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  value={login.password}
                  name="password"
                  onChange={this.handleChange}
                />
                {errors.password ? (
                  <div className="text-danger">{errors.password}</div>
                ) : (
                  ""
                )}
              </div>

              <div className="form-group">
                <label htmlFor="repassword">Confirm Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="repassword"
                  value={login.repassword}
                  name="repassword"
                  onChange={this.handleChange}
                />
                {errors.repassword ? (
                  <div className="text-danger">{errors.repassword}</div>
                ) : (
                  ""
                )}
              </div>

              <div className="form-group">
                <button className="btn btn-primary">Register</button>
              </div>

              <div className="form-group">
                <Link className="text-primary" to="/login">
                  Login Existing user
                </Link>
              </div>
            </form>
          </div>
          <div className="col-4"></div>
        </div>
      </div>
    );
  }
}
export default Register;
